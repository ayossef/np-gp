# Install PGAdmin using Docker 

## First install Docker 

In order to intall docker we have two options:
1. Install directly using APT via ubuntu servers
2. Install using APT via docker servers


we are using the second option, why?
because, the most recent versions are avaialble on docker servers first.
1. Update the local repo
```bash
sudo apt update
```

2. Install some package for verifiction
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

3. Add the keys of docker servers
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
#verify keys adding
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

4. Install docker using apt via docker servers
```bash
sudo apt update
sudo apt install docker-ce
```

## Second run pgadmin on top docker
```bash
docker run dpage/pgadmin4
```