# Installing PostgreSQL

## Update the local package manager (apt)
```bash
sudo apt update
```

## Use the package manager (apt) to download and install Postgres

```bash
sudo apt install postgesql
```

## Check PostgreSQL service status
```bash
sudo systemctl status postresql
```

## Connecting to PostgreSQL using the CLI Interface
```bash
# Fist you need to switch to postgres user
sudo su postgres
# Second you can connect to postgresql server
psql
```
